## Sneakers test

### install like this:


```ruby
bundle install
bundle exec rake db:create
bundle exec rake db:migrate
```

### start the worker:

`bundle exec rake sneakers:run` 

### see if it's running:

`tail -f log/sneakers.log`

### some one liners

`bundle exec rails c`

  - `MessagePublisher.publish("demo.demo", "hello world")`
  - `MessagePublisher.publish("demo.simple", "hello world")`
  - `MessagePublisher.publish("demo.suprise", "hello world")`
  - `MessagePublisher.remote_call("demo.rpc_server", 102)`
  - `MessagePublisher.remote_call("demo.posts_count", 0)`
